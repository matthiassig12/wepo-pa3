// Taken from http://jonsamwell.com/url-route-authorization-and-security-in-angular/

angular.module("EvalApp").factory("AuthService", ["Authentication",
function(Authentication) {
	var authorize = function (loginRequired, requiredPermissions, permissionCheckType) {
		var result = "authorized", 
			user = Authentication.getCurrentLoginUser(),
			loweredPermissions = [],
			hasPermission = true,
			permission, i;

		permissionCheckType = permissionCheckType || "atLeastOne";
		if (loginRequired === true && user === undefined) {
			result = "loginRequired";
		}
		else if ((loginRequired === true && user !== undefined) &&
				 (requiredPermissions === undefined || requiredPermissions.length === 0)) {
			result = "authorized";
		}
		else if (requiredPermissions) {
			loweredPermissions = [];
			angular.forEach(user.permissions, function (permission) {
				loweredPermissions.push(permission.toLowerCase());
			});

			for (i = 0; i < requiredPermissions.length; i += 1) {
				permission = requiredPermissions[i].toLowerCase();

				if (permissionCheckType === "combinationRequired") {
					hasPermission = hasPermission && loweredPermission.indexOf(permission) > -1;

					// if all the permissions are required and hasPermission is false there is no point carrying on
					if (hasPermission === false) {
						break;
					}
				}
				else if (permissionCheckType === "atLeastOne") {
					hasPermission = loweredPermissions.indexOf(permission) > -1;

					// if we only need one of the permissions and we have it there is no point carrying on
	                if (hasPermission) {
	                    break;
	                }
				}
			}

			result = hasPermission ? "authorized" : "notAuthorized";
		}
		return result;
	};
	return {
		authorize: authorize
	};
}]);