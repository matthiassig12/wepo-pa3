angular.module("EvalApp").factory("TemplateResource", ["$http", 
function($http) {
	return {
		createTemplate: function(template) {
			return $http.post("http://dispatch.ru.is/h10/api/v1/evaluationtemplates", template)
		        .then(function(response){
		        	return response.data;
		        });
		},
		getAllTemplates: function() {
			return $http.get("http://dispatch.ru.is/h10/api/v1/evaluationtemplates")
				.then(function(response) {
					return response.data;
				});
		},
		getTemplateByID: function(id) {
			return $http.get("http://dispatch.ru.is/h10/api/v1/evaluationtemplates/" + id)
				.then(function(response) {
					return response.data;
				})
		},
		getNewTemplate: function(title, titleEN, introText, introTextEN) {
			var t = {
				Title: title,
				TitleEN: "hello",
				IntroText: introText,
				IntroTextEN: introTextEN,
				CourseQuestions: [],
				TeacherQuestions: []
			};
			return t;
		}
		// Skoða $http documentation í angular
		// Geyma validation token í service objecti, sem passar uppa ad tad fylgi
		// ollum fyrirspurnum
		// $http.get("url/to/api").then(function(response)){	
		// $scope.templates = response.data;
		// Hér höfum við aðgang að http header
		//}
		// EÐA:
		// $http.get("url/to/api").success(function(data)){
		//	$scope.templates = data;
		//}
	}
}]);