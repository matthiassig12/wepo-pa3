angular.module("EvalApp").controller("LoginController", ["$scope", "$location", "Authentication", "toaster",
function($scope, $location, Authentication, toaster) {
	// TODO: ANgular/HTML5 validation
	$scope.username = "";
	$scope.password = "";
	$scope.login = function() {
		$scope.invalidLogin = false;
		$scope.isBusy = true;
		Authentication.login($scope.username, $scope.password).then(function() {
			$location.path("/index");
		}, function(data) {
			toaster.pop('warning', data, "Please try again");
			$scope.invalidLogin = true;
		})["finally"](function () {
			$scope.isBusy = false;
		});
	};
}]);