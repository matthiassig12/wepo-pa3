angular.module("EvalApp").controller("questionController", ["$scope", "EvaluationResource","$routeParams","$location",
function($scope, EvaluationResource,$routeParams,$location) {

	var course = $routeParams.courseId;
	var sem = $routeParams.semester;
	var id = $routeParams.evalId;

	$scope.questions = [];
	$scope.title = ""; 
	$scope.getTeachersInCourseToEvaluate = [];
	$scope.teacherAns = [];

	EvaluationResource.getEvaluationById(course,sem,id).then(function(data) {			

		$scope.questions = data;
		$scope.title = data.Title;

	}); 

	EvaluationResource.getTeachersInCourse(course,sem).then(function(data){

		$scope.getTeachersInCourseToEvaluate = data;
		
		createTeacherQuestions();

	});

	function createTeacherQuestions () {
			
			for(var i = 0; i < $scope.getTeachersInCourseToEvaluate.length; i++){
				
				var obj = {
						
							name: $scope.getTeachersInCourseToEvaluate[i].FullName,
							ssn:$scope.getTeachersInCourseToEvaluate[i].SSN,
							questions: []
						};

				for(var j = 0; j < $scope.questions.TeacherQuestions.length; j++){

						var o = clone($scope.questions.TeacherQuestions[j]);
						o.Answers = [];
					for(var y = 0; y < $scope.questions.TeacherQuestions[j].Answers.length; y ++){

						var ob = clone($scope.questions.TeacherQuestions[j].Answers[y]);
						o.Answers.push(ob);
					}

						obj.questions.push(o);
				}
					
				$scope.teacherAns.push(obj);
			}	

	}

	// this function is borrowed from 
	//http://stackoverflow.com/questions/728360/most-elegant-way-to-clone-a-javascript-object
	function clone(obj) {
	    if (null == obj || "object" != typeof obj) return obj;
	    var copy = obj.constructor();
	    for (var attr in obj) {
	        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
	    }
	    return copy;
	}

	$scope.submitEval = function(){
		
		var subMitObj = [];

		for(var i = 0; i < $scope.questions.CourseQuestions.length; i++ ){

			var courseId = $scope.questions.CourseQuestions[i].ID;

			if($scope.questions.CourseQuestions[i].Type === 'multiple'){
			

				var multAnswers = "";
				for(var j = 0; j < $scope.questions.CourseQuestions[i].Answers.length; j++){
					
					if($scope.questions.CourseQuestions[i].Answers[j].ansArr === true){
		

						multAnswers +=  $scope.questions.CourseQuestions[i].Answers[j].ID + ",";
					}

				}
				
				multAnswers =  multAnswers.substring(0, multAnswers.length - 1);
				
				obj = {
					QuestionID: courseId,
   				    TeacherSSN: "",
   					Value: multAnswers	
				};

				subMitObj.push(obj);
			
			}else
				{	
					var courseAnswer = $scope.questions.CourseQuestions[i].Answer;

				obj = {
					QuestionID: courseId,
   				    TeacherSSN: "",
   					Value: courseAnswer
				};

				subMitObj.push(obj);
			}	
	
		}

		// for teachers
		for(var i = 0; i < $scope.teacherAns.length; i++ ){

				var teacherSSN = $scope.teacherAns[i].ssn;

				

				for(var k = 0; k < $scope.teacherAns[i].questions.length; k++){
							questId = $scope.teacherAns[i].questions[k].ID;
						if($scope.teacherAns[i].questions[k].Type !== 'multiple'){

							var qId = $scope.teacherAns[i].questions[k].ID;
							var ans = $scope.teacherAns[i].questions[k].Answer;

							obj = {
								QuestionID: questId,
   				    			TeacherSSN: teacherSSN,
   								Value: ans
							};

							subMitObj.push(obj);
						}
						//multiple
						else{
							var m = "";
							for(var j = 0; j < $scope.teacherAns[i].questions[k].Answers.length; j++){
								
								if($scope.teacherAns[i].questions[k].Answers[j].ansArr === true){
									
									m += $scope.teacherAns[i].questions[k].Answers[j].ID+',';

								}
							}
							m = m.substring(0, m.length - 1);
							

							obj = {
								QuestionID: questId,
   				    			TeacherSSN: teacherSSN,
   								Value: m
							};

							subMitObj.push(obj);

						}

				}
	
		}
		// post to server here 
		EvaluationResource.postEval(subMitObj,course,sem,id).then(function(data){
			
		} );
		$location.path('/views/home');
	}

	
}]);