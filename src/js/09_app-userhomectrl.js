angular.module("EvalApp").controller("UserHomeController", ["$scope", "EvaluationResource",
function($scope, EvaluationResource) {

	$scope.evaluations = [];

	EvaluationResource.getEvaluations().then(function(data) {

		$scope.evaluations  = data;
		
	});

}]);