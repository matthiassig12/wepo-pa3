angular.module("EvalApp").controller("AdminHomeController", ["$scope", "TemplateResource", "toaster", "$q", "EvaluationResource",
function($scope, TemplateResource, toaster, $q, EvaluationResource) {
	$scope.canEdit = false;
	$scope.newTitle = "";
	$scope.newIntro = "";
	$scope.newQuestion = {
		Type: "text",
		Text: "",
		TextEN: "",
		ImageURL: ""
	};
	//$scope.questionType = "";
	$scope.isCourseQuestion = true;
	//$scope.questionText = "";
	$scope.answers = {
		first: "",
		second: "",
		third: "",
		fourth: "",
		fifth: ""
	};

	$scope.showTeacherQuestions = false;
	$scope.canAssign = false;

	$scope.date = 
	{
		opened : {},
  		open : function($event) {

		    $event.preventDefault();
		    $event.stopPropagation();

		    $scope.date.opened = {};
		    $scope.date.opened[$event.target.id] = true;

		    // log this to check if its setting the log    
    
  		},
  		dtFrom: "",
  		dtTo: ""
  	};

  self.format = 'dd-MM-yyyy'

	$scope.getAllTemplates = function() {
		var deferred = $q.defer();
		TemplateResource.getAllTemplates().then(function(data) {
			$scope.templates = data;
			deferred.resolve([]);
		});
		return deferred.promise;
	};

	$scope.getTemplate = function(template) {
		TemplateResource.getTemplateByID(template.ID).then(function(data) {
			$scope.template = data;
		});
		$scope.canEdit = true;
		$scope.canAssign = true;
	};

	$scope.setQuestionType = function(type) {
		$scope.newQuestion.Type = type;
	};

	$scope.setCourseQuestion = function(bool) {
		$scope.isCourseQuestion = bool;
	};

	$scope.createNewTemplate = function() {
		var promise = $scope.getAllTemplates();
		promise.then(function() {
			if (templateNameExists($scope.newTitle)) {
				toaster.pop('warning', "Template " + $scope.newTitle + " already exists", "Please rename your template");
				return;
			}
			if ($scope.newIntro === "" || $scope.newTitle === "") {
				toaster.pop('warning', "Missing title or intro", "Please fill in all fields");
				return;
			}
			$scope.template = TemplateResource.getNewTemplate($scope.newTitle, "English title", $scope.newIntro, "English intro");
			$scope.canEdit = true;
			$scope.canAssign = false;
		});
	};

	$scope.addQuestion = function() {
		if ($scope.newQuestion.Type === "single" || $scope.newQuestion.Type === "multiple") {
			$scope.newQuestion.Answers = [];
			if ($scope.answers.first !== "") $scope.newQuestion.Answers.push({ID: 0, Text: $scope.answers.first, TextEN: "", ImageURL: "", Weight: 1});
			if ($scope.answers.second !== "") $scope.newQuestion.Answers.push({ID: 1, Text: $scope.answers.second, TextEN: "", ImageURL: "", Weight: 2});
			if ($scope.answers.third !== "") $scope.newQuestion.Answers.push({ID: 2, Text: $scope.answers.third, TextEN: "", ImageURL: "", Weight: 3});
			if ($scope.answers.fourth !== "") $scope.newQuestion.Answers.push({ID: 3, Text: $scope.answers.fourth, TextEN: "", ImageURL: "", Weight: 4});
			if ($scope.answers.fifth !== "") $scope.newQuestion.Answers.push({ID: 4, Text: $scope.answers.fifth, TextEN: "", ImageURL: "", Weight: 5});
			if ($scope.newQuestion.Answers.length === 0) {
				toaster.pop('warning', "No answer choices", "Please add some possible answers");
				return;
			}
		}
		if ($scope.newQuestion.Text === "") {
			toaster.pop('warning', "No question specified", "Please add some text for the question");
			return;
		}
		var cloneObj = JSON.parse( JSON.stringify( $scope.newQuestion ) );
		$scope.isCourseQuestion ? $scope.template.CourseQuestions.push(cloneObj)
								: $scope.template.TeacherQuestions.push(cloneObj);
		$scope.canAssign = false;
		$scope.answers.first = "";
		$scope.answers.second = "";
		$scope.answers.third = "";
		$scope.answers.fourth = "";
		$scope.answers.fifth = "";
		$scope.newQuestion = {
			Type: "text",
			Text: "",
			TextEN: "",
			ImageURL: ""
		};
	};

	$scope.removeQuestion = function(q, rmFromCourse) {
		var arr;
		if (rmFromCourse) {
			arr = $scope.template.CourseQuestions;
		}
		else {
			arr = $scope.template.TeacherQuestions;
		}
		var index = arr.indexOf(q);
		if (index > -1) {
			arr.splice(index, 1);
		}
	};

	$scope.setShowTeacherQuestions = function(val) {
		$scope.showTeacherQuestions = val;
	};

	$scope.save = function() {
		var promise = $scope.getAllTemplates();
		promise.then(function() {
			if (templateNameExists($scope.template.Title)) {
				toaster.pop('warning', "Template " + $scope.template.Title + " already exists", "Please rename your template");
				return;
			}
			if ($scope.template.CourseQuestions.length + $scope.template.TeacherQuestions.length === 0) {
				toaster.pop('warning', "No questions", "Please add some questions to your template");
			}
			else {
				TemplateResource.createTemplate($scope.template).then(function(data) {
					toaster.pop('success', "Success!", $scope.template.Title + " successfully saved.");
				},
				function(response) {
					toaster.pop('error', "Error", "Could not save " + $scope.template.Title);
				}
				);
			}
		});
	};

	$scope.discard = function() {
		$scope.template = {};
		$scope.canEdit = false;
	};

	function templateNameExists(name) {
		for (var i = 0; i < $scope.templates.length; i++) {
			if (name === $scope.templates[i].Title) {
				return true;
			}
		}
		return false;
	};

	$scope.assign = function() {
		EvaluationResource.assignEvaluation($scope.template.ID, $scope.date.dtFrom, $scope.date.dtTo).then(function(data) {
			console.log(data);
		});
	};

	$scope.rename = function(newName) {
		$scope.template.Title = newName;
	};

}]);