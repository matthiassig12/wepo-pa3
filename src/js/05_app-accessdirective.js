// Taken from http://jonsamwell.com/url-route-authorization-and-security-in-angular/

angular.module("EvalApp").directive("access", ["AuthService",
function(AuthService) {
	return {
		restrict: "A",
		link: function (scope, element, attrs) {
			var makeVisible = function () {
				element.removeClass("hidden");
				},
				makeHidden = function () {
					element.addClass("hidden");
				},
				determineVisibility = function (resetFirst) {
					var result;
					if (resetFirst) {
						makeVisible();
					}

					result = AuthService.authorize(true, roles, attrs.accessPermissionType);
					if (result === "authorized") {
						makeVisible();
					}
					else {
						makeHidden();
					}
				},
				roles = attrs.access.split(",");

			if (roles.length > 0) {
				determineVisibility(true);
			}
		}
	};
}]);