angular.module("EvalApp").factory("EvaluationResource", ["$http", 
function($http) {
	return {
		getEvaluations: function() {
			return $http.get("http://dispatch.ru.is/h10/api/v1/my/evaluations")
				.then(function(response) {
					return response.data;
				});
		},
		getEvaluationResults: function(id) {
			return $http.get("http://dispatch.ru.is/h10/api/v1/evaluations/" + id)
				.then(function(response) {
					return response.data;
				})
		},
		getEvaluationById: function(course,semester,id) {			

			var url = "http://dispatch.ru.is/h10/api/v1/courses/" + course + "/" + semester + "/evaluations/" + id; 
			
			return $http.get(url)
				.then(function(response) {
					return response.data;
				}); 
		},
		getTeachersInCourse: function(course,semester){
			var url = "http://dispatch.ru.is/h10/api/v1/courses/" + course + "/" + semester + "/teachers/"; 
			
			return $http.get(url)
				.then(function(response) {
					return response.data;
				}); 
		},
		assignEvaluation: function(id, from, to) {
			return $http.post("http://dispatch.ru.is/h10/api/v1/evaluations", {TemplateID: id, StartDate: from, EndDate: to})
		        .then(function(response){
		        	return response.data;
		        });
		},
		postEval: function(eval,course,sem,id) {

			var url = "http://dispatch.ru.is/h10/api/v1/courses/" + course + "/" + sem + "/evaluations/" + id; 
			return $http.post(url, eval)
		        .then(function(response){
		        	return response.data;
		        });
		},
	}
}]);