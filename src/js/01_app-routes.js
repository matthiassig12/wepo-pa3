"use strict";

angular.module("EvalApp", ["ng", "ngRoute", "dndLists", "toaster", "ui.bootstrap", "googlechart"])
.config(["$routeProvider", "$locationProvider",
function($routeProvider, $locationProvider){

	$routeProvider.when("/login", {
		templateUrl: "views/login.html",
		controller: "LoginController"
	}).when("/admin/index", {
		templateUrl: "/views/adminindex.html",
		controller: "AdminHomeController",
		access: {
			requiresLogin: true,
			requiredPermissions: ["Admin"],
			permissionType: "atLeastOne" // Has to have at least one of the permission specified
		}
	}).when("/admin/myresults", {
		templateUrl: "/views/adminresults.html",
		controller: "AdminResultsController",
		access: {
			requiresLogin: true,
			requiredPermissions: ["Admin"],
			permissionType: "atLeastOne" // Has to have at least one of the permission specified
		}
	}).when("/index", {
		templateUrl: "/views/home.html",
		controller: "UserHomeController",
		access: {
			requiresLogin: true,
			requiredPermissions: ["User"],
			permissinoType: "atLeastOne"
		}
	}).when("/questions/:courseId/:semester/:evalId", {
		templateUrl:"/views/questions.html",
		controller: "questionController",
		access: {
			requiresLogin: true,
			requiredPermissions: ["User"],
			permissionType: "atLeastOne"
		}
	}).otherwise({ redirectTo: "/index"});

}]);