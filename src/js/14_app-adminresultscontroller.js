angular.module("EvalApp").controller("AdminResultsController", ["$scope", "EvaluationResource",
function($scope, EvaluationResource) {
	$scope.showResults = false;
	$scope.charts = {};
	$scope.showCourseQuestions = true;
	$scope.SSNToTeacher = {

	};

	$scope.closedEvaluations = [];
	EvaluationResource.getEvaluationResults("").then(function(data) {
		for (var i = 0; i < data.length; i++) {
			if (data[i].Status === "open") {
				EvaluationResource.getEvaluationResults(data[i].ID).then(function(data) {
					$scope.closedEvaluations.push(data);
				});
			}
		}
	});


	$scope.setCurrentEval = function(e) {
		$scope.currentEvaluation = e;
		setSSNToTeacher($scope.currentEvaluation);
		if ($scope.currentEvaluation.Courses.length > 0) {
			for (var i = 0; i < $scope.currentEvaluation.Courses.length; i++) {
				for (var j = 0; j < $scope.currentEvaluation.Courses[i].Questions.length; j++) {
					var question = $scope.currentEvaluation.Courses[i].Questions[j];
					if (question.Type === "single" || question.Type === "multiple") {
						attrs = [["Option", "total answers"]];
						for (var k = 0; k < question.OptionsResults.length; k++) {
							attrs.push([question.OptionsResults[k].AnswerText, question.OptionsResults[k].Count]);
						}
						courseID = $scope.currentEvaluation.Courses[i].CourseID;
						if ($scope.charts[courseID] === undefined) {
							$scope.charts[courseID] = {};
						}
            if ($scope.charts[courseID][question.TeacherSSN] === undefined) {
              $scope.charts[courseID][question.TeacherSSN] = {};
            }
						$scope.charts[courseID][question.TeacherSSN][question.QuestionID] = {
							"type": "PieChart",
							  "data": attrs,
							  "options": {
							    "displayExactValues": true,
							    "width": 400,
							    "height": 200,
							    "is3D": true,
							    "chartArea": {
							      "left": 10,
							      "top": 10,
							      "bottom": 0,
							      "height": "100%"
							    }
							  },
							  "displayed": true
						};

					}
				}
			}
			$scope.showResults = true;
		}
		else {
			$scope.showResults = false;
		}
		$scope.currentCourse = undefined;
	};

	$scope.setCurrentCourse = function(course) {
		$scope.currentCourse = course;
	};

	$scope.setShowCourseQuestions = function(val) {
		$scope.showCourseQuestions = val;
	};

	function setSSNToTeacher(eval) {
		for (var i = 0; i < eval.Courses.length; i++) {
			EvaluationResource.getTeachersInCourse(eval.Courses[i].CourseID, eval.Courses[i].Semester)
				.then(function(data) {
					for (var j = 0; j < data.length; j++) {
						$scope.SSNToTeacher[data[j].SSN] = data[j].FullName;
					}
				});
		}
	}

}]);