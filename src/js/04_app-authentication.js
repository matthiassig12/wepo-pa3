// Taken from http://jonsamwell.com/url-route-authorization-and-security-in-angular/

angular.module("EvalApp").factory("Authentication", ["$q", "$timeout", "$http", "EventBus",
function($q, $timeout, $http, EventBus) {
	var currentUser,
		createUser = function (name, permissions, token) {
			return {
				name: name,
				permissions: permissions,
				token: token
			};
		},
		login = function (username, password) {
			var defer = $q.defer();
	        $timeout(function () {
		        username = username.toLowerCase();
		        var userData;
		        $http.post("http://dispatch.ru.is/h10/api/v1/login", {user: username, pass: password})
		        .success(function(data) {
		        	if (data.User.Role === "admin") {
			            currentUser = createUser("Admin User", ["Admin"], data.Token);
			        } else if (data.User.Role === "student") {
			            currentUser = createUser("Normal User", ["User"], data.Token);
			        } else {
			            defer.reject("Unknown Username / Password combination");
		            return;
		        	}
		        	$http.defaults.headers.common.Authorization = "Basic " + data.Token;

		        defer.resolve(currentUser);
		        EventBus.broadcast("userLoggedIn", currentUser);
		    	}, 1000)
		    	.error(function () {
		    		defer.reject("Unknown Username / Password combination");
		    	});
		        })

		    	return defer.promise;
	    },
	    logout = function() {
	    	// we should only remove the current user.
	        // routing back to login login page is something we shouldn't
	        // do here as we are mixing responsibilities if we do.
	        currentUser = undefined;
	        EventBus.broadcast("loggedOut");
	    },
	    getCurrentLoginUser = function() {
	    	return currentUser;
	    };

	return {
		login: login,
		logout: logout,
		getCurrentLoginUser: getCurrentLoginUser
	};
}]);