// Taken from http://jonsamwell.com/url-route-authorization-and-security-in-angular/

angular.module("EvalApp").run(["$rootScope", "$location", "AuthService", "Authentication",
function($rootScope, $location, AuthService, Authentication) {
	var routeChangeRequiredAfterLogin = false,
	loginRedirectUrl;
	$rootScope.$on("$routeChangeStart", function(event, next) {
		var authorized;
		if (routeChangeRequiredAfterLogin && next.originalPath !== "/login") {
			var user = Authentication.getCurrentLoginUser();
			routeChangeRequiredAfterLogin = false;
			if (user.permissions.indexOf("Admin") !== -1) {
				loginRedirectUrl = "/admin/index";
			}
			$location.path(loginRedirectUrl).replace();
		}
		else if (next.access !== undefined) {
			authorized = AuthService.authorize(next.access.requiresLogin, next.access.requiredPermissions, next.permissionType);
			if (authorized === "loginRequired") {
				routeChangeRequiredAfterLogin = true;
				loginRedirectUrl = next.originalPath;
				$location.path("/login");
			}
			else if (authorized === "notAuthorized") {
				var user = Authentication.getCurrentLoginUser();
				user.permissions.indexOf("Admin") !== -1 ? $location.path("/admin/index").replace() : $location.path("/notauthorized").replace();
				
			}
		}
	})
}]);