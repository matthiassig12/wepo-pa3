describe("AdminHomeController", function() {

	var controller;
	var scope;
	/*var mockBackend;
	var mockTemplateResource = {
		createTemplate: function(template){
			return mockBackend.post("evaluationtemplates", template)
				.then(function(response) {
					return response.data;
				});
		},
		getAllTemplates: function() {
			return mockBackend.get("getalltemplates")
				.then(function(response) {
					return response.data;
				})
		}
	};

	var mockEvaluationResource = {
		getEvaluations: function(){
			return mockBackend.get("my_evaluations")
				.then(function(response) {
					return response.data;
				});
		}
	};*/

	//TODO: Testa save og assign og createNewTemplate og gettemplate og getalltemplates


	beforeEach(module("EvalApp"));

	beforeEach(inject(function($controller, $rootScope) {

		scope = $rootScope.$new();
		/*mockBackend = _$httpBackend_;


		mockBackend.whenPOST("evaluationstemplates")
			.respond([
				{
					response: "Success"
				}]);
		mockBackend.whenGET("my_evaluations")
			.respond([
				{
					response: "Success"
				}]);

		mockBackend.whenGET("my_evaluations")
			.respond([
				{
					Title: "someothername",
					TitleEN: "testEN",
					IntroText: "test",
					IntroTextEN: "testEN",
					CourseQuestions: [],
					TeacherQuestions: []
				},
				{
					Title: "somename",
					TitleEN: "testEN",
					IntroText: "test",
					IntroTextEN: "testEN",
					CourseQuestions: [],
					TeacherQuestions: []
				}
				]);
		spyOn(mockTemplateResource, "getAllTemplates").and.returnValue(deferred.promise);
		
		spyOn(mockTemplateResource, "createTemplate").andReturnValue($q.when());*/
		controller = $controller("AdminHomeController", {
			$scope: scope,
			//TemplateResource: mockTemplateResource,
			//EvaluationResource: mockEvaluationResource
		});

	}));

	it("should initialize objects", function() {
		expect(scope.canEdit).toBeDefined();
		expect(scope.newTitle).toBeDefined();
		expect(scope.newIntro).toBeDefined();
		expect(scope.newQuestion).toBeDefined();
		expect(scope.isCourseQuestion).toBeDefined();
		expect(scope.answers).toBeDefined();
		expect(scope.showTeacherQuestions).toBeDefined();
		expect(scope.canAssign).toBeDefined();
		expect(scope.date).toBeDefined();

	});

	it("should correctly set the type of a new question", function() {
		scope.setQuestionType("a");
		expect(scope.newQuestion.Type).toEqual("a");
	});

	it("should correctly set the truth value of isCourseQuestion", function() {
		scope.setCourseQuestion(true);
		expect(scope.isCourseQuestion).toBe(true);
		scope.setCourseQuestion(false);
		expect(scope.isCourseQuestion).toBe(false);
	});

	it("should add a single course text question correctly", function() {
		scope.setCourseQuestion(true);
		scope.newQuestion = {
			Type: "text",
			Text: "test",
			TextEN: "testEN",
			ImageURL: "testIMG"
		};
		scope.template = {
			Title: "test",
			TitleEN: "testEN",
			IntroText: "test",
			IntroTextEN: "testEN",
			CourseQuestions: [],
			TeacherQuestions: []
		};
		scope.addQuestion();
		expect(scope.template.CourseQuestions.length).toEqual(1);
		expect(scope.template.TeacherQuestions.length).toEqual(0);
	});

	it("should add a single teacher text question correctly", function() {
		scope.setCourseQuestion(false);
		scope.newQuestion = {
			Type: "text",
			Text: "test",
			TextEN: "testEN",
			ImageURL: "testIMG"
		};
		scope.template = {
			Title: "test",
			TitleEN: "testEN",
			IntroText: "test",
			IntroTextEN: "testEN",
			CourseQuestions: [],
			TeacherQuestions: []
		};
		scope.addQuestion();
		expect(scope.template.TeacherQuestions.length).toEqual(1);
		expect(scope.template.CourseQuestions.length).toEqual(0);
	});

	it("should NOT add a question without a text", function() {
		scope.setCourseQuestion(false);
		scope.newQuestion = {
			Type: "text",
			Text: "",
			TextEN: "testEN",
			ImageURL: "testIMG"
		};
		scope.template = {
			Title: "test",
			TitleEN: "testEN",
			IntroText: "test",
			IntroTextEN: "testEN",
			CourseQuestions: [],
			TeacherQuestions: []
		};
		scope.addQuestion();
		expect(scope.template.TeacherQuestions.length + scope.template.CourseQuestions.length).toEqual(0);
	});

	it("should add a single course single choice question correctly", function() {
		scope.setCourseQuestion(false);
		scope.newQuestion = {
			Type: "single",
			Text: "test",
			TextEN: "testEN",
			ImageURL: "testIMG"
		};
		scope.template = {
			Title: "test",
			TitleEN: "testEN",
			IntroText: "test",
			IntroTextEN: "testEN",
			CourseQuestions: [],
			TeacherQuestions: []
		};
		scope.answers.first = "test";
		scope.addQuestion();
		expect(scope.template.TeacherQuestions.length).toEqual(1);
		expect(scope.template.CourseQuestions.length).toEqual(0);
	});

	it("should NOT add a single choice question without any answer possibilites", function() {
		scope.setCourseQuestion(false);
		scope.newQuestion = {
			Type: "single",
			Text: "test",
			TextEN: "testEN",
			ImageURL: "testIMG"
		};
		scope.template = {
			Title: "test",
			TitleEN: "testEN",
			IntroText: "test",
			IntroTextEN: "testEN",
			CourseQuestions: [],
			TeacherQuestions: []
		};
		scope.addQuestion();
		expect(scope.template.TeacherQuestions.length).toEqual(0);
		expect(scope.template.CourseQuestions.length).toEqual(0);
	});

	it("should reset the answers object after adding a question", function() {
		scope.setCourseQuestion(false);
		scope.newQuestion = {
			Type: "single",
			Text: "test",
			TextEN: "testEN",
			ImageURL: "testIMG"
		};
		scope.template = {
			Title: "test",
			TitleEN: "testEN",
			IntroText: "test",
			IntroTextEN: "testEN",
			CourseQuestions: [],
			TeacherQuestions: []
		};
		scope.answers.first = "test";
		scope.answers.second = "test";
		scope.answers.third = "test";
		scope.answers.fourth = "test";
		scope.answers.fifth= "test";
		scope.addQuestion();
		expect(scope.answers.first).toEqual("");
		expect(scope.answers.second).toEqual("");
		expect(scope.answers.third).toEqual("");
		expect(scope.answers.fourth).toEqual("");
		expect(scope.answers.fifth).toEqual("");
	});


	it("should remove a course question correctly", function() {
		var q = {
			Type: "single",
			Text: "test",
			TextEN: "testEN",
			ImageURL: "testIMG"
		};
		scope.template = {
			Title: "test",
			TitleEN: "testEN",
			IntroText: "test",
			IntroTextEN: "testEN",
			CourseQuestions: [q],
			TeacherQuestions: []
		};
		scope.removeQuestion(q, true);
		expect(scope.template.CourseQuestions.length).toEqual(0);
	});

	it("should remove a teacher question correctly", function() {
		var q = {
			Type: "single",
			Text: "test",
			TextEN: "testEN",
			ImageURL: "testIMG"
		};
		scope.template = {
			Title: "test",
			TitleEN: "testEN",
			IntroText: "test",
			IntroTextEN: "testEN",
			CourseQuestions: [],
			TeacherQuestions: [q]
		};
		scope.removeQuestion(q, false);
		expect(scope.template.TeacherQuestions.length).toEqual(0);
	});

	it("should correctly set showTeacherQuestions", function() {
		scope.setShowTeacherQuestions(true);
		expect(scope.showTeacherQuestions).toBe(true);
		scope.setShowTeacherQuestions(false);
		expect(scope.showTeacherQuestions).toBe(false);
	});

	it("should correctly discard a template", function() {
		scope.template = {
			Title: "test",
			TitleEN: "testEN",
			IntroText: "test",
			IntroTextEN: "testEN",
			CourseQuestions: [],
			TeacherQuestions: []
		};
		scope.canEdit = true;
		scope.discard();
		expect(scope.template).toEqual({});
		expect(scope.canEdit).toBe(false);
	});

	it("should NOT create a template if there exists another one with the same name", function() {

		scope.newTitle = "identicalname";

		var t1 = {
			Title: "identicalname",
			TitleEN: "testEN",
			IntroText: "test",
			IntroTextEN: "testEN",
			CourseQuestions: [],
			TeacherQuestions: []
		};

		var t2 = {
			Title: "someothername",
			TitleEN: "testEN",
			IntroText: "test",
			IntroTextEN: "testEN",
			CourseQuestions: [],
			TeacherQuestions: []
		};

		scope.templates = [t1, t2];
		scope.createNewTemplate();
		//expect(mockTemplateResource.createTemplate).not.toHaveBeenCalled();
		expect(scope.template).toBe(undefined);
	});

	it("should rename a template correctly", function() {
		scope.template = {
			Title: "test",
			TitleEN: "testEN",
			IntroText: "test",
			IntroTextEN: "testEN",
			CourseQuestions: [],
			TeacherQuestions: []
		};
		scope.rename("newname");
		expect(scope.template.Title).toEqual("newname");
	});

	//TODO: Testa save og assign og createNewTemplate og gettemplate og getalltemplates


});

describe("AdminResultsController", function() {

	var controller;
	var scope;




	beforeEach(module("EvalApp"));

	beforeEach(inject(function($controller, $rootScope) {

		scope = $rootScope.$new();

		controller = $controller("AdminResultsController", {
			$scope: scope,
		});

	}));

	it("should initialize objects", function() {
		expect(scope.showResults).toBeDefined();
		expect(scope.charts).toBeDefined();
		expect(scope.showCourseQuestions).toBeDefined();
		expect(scope.SSNToTeacher).toBeDefined();
		expect(scope.closedEvaluations).toBeDefined();

	});



});


describe("questionController", function() {

		
	var controller;
	var scope;




	beforeEach(module("EvalApp"));

	beforeEach(inject(function($controller, $rootScope) {

		scope = $rootScope.$new();

		controller = $controller("questionController", {
			$scope: scope,
		});

	}));


	it("should initialize a  objects ", function() {

		expect(scope.questions).toBeDefined();
		expect(scope.title).toBeDefined();
		expect(scope.getTeachersInCourseToEvaluate).toBeDefined();
		expect(scope.teacherAns).toBeDefined();


	});



/*
	var mockEvaluationResource = {
		getEvaluationById: function(course,sem,id){
				
		}
	};

	beforeEach(module("EvalApp"));

	beforeEach(inject(function($controller, $rootScope) {

		scope = $rootScope.$new();

		//spyOn(mockEvaluationResource, "getEvaluationById");

		controller = $controller("questionController", {

			$scope: scope,
			backEnd:mockEvaluationResource,
			
		});

	}));


	it("have called getEvaluationById with course,sem,id", function() {
		
		var c = "wepo";
		var s = "haust";
		var i = 1;

		expect(mockEvaluationResource.getEvaluationById(c,s,i)).toHaveBeenCalledWith(c,s,i);

	}); 
*/


});
